FROM nextcloud:apache

RUN apt-get clean && apt-get update && apt-get install -y \
    supervisor sudo \
    ocrmypdf tesseract-ocr-eng \
    cups-client cups-daemon cups-bsd \
  && rm -rf /var/lib/apt/lists/* \
  && mkdir /var/log/supervisord /var/run/supervisord

COPY supervisord.conf /

ENV NEXTCLOUD_UPDATE=1

CMD ["/usr/bin/supervisord", "-c", "/supervisord.conf"]
